<?php
include '../adapter/connection.php';
include '../model/select.php';

$connection->set_charset('utf8');


///////// Valores iniciales ////////////////

$salida="";
$fecha=strftime( "%Y-%m-%d %H:%M:%S", time() );
$query="SELECT * FROM users ORDER BY ID desc";


if(isset($_POST['consultaReporte'])){
$q = $connection->real_escape_string($_POST['consultaReporte']);

$query = "SELECT * FROM users WHERE (name LIKE '%".$q."%' OR code LIKE '%".$q."%') ORDER BY ID desc"; 
}



$search = $connection->query($query);

if ($search ->num_rows > 0)
{

$salida.= '
<div class="container">
<table class="responsive-table striped">
    <thead>
      <tr>
          <th>Nombre</th>
          <th>Turno</th>
          <th>Codigo</th>
          <th>Fecha de Entrada</th>
          <th>Imagen</th>
          <th></th>
      </tr>
    </thead>

    <tbody>';
    
  while ($row = $search->fetch_assoc()) {
  //  if ($nuevafecha <= $row['login']) {
    $salida.=
      ' <tr>
          <td>'.$row['name'].'</td>
          <td>'.$row['turn'].'</td>
          <td>'.$row['code'].'</td>
          <td>'.$row['registry'].'</td>
          <td><img width="40px" src="'.$row['picture'].'"></td>
          <td><a class="btn orange darken-3 waves-effect waves-teal" onclick="edit('.$row['code'].');"><i class="material-icons">create</i></a></td>
        </tr>';
  //  }
  }
 $salida.= '
    </tbody>
  </table>';
}else{
    while ($row = $search->fetch_assoc()) {
  //  if ($nuevafecha <= $row['login']) {
    $salida.=
      ' <tr>
          <td>'.$row['name'].'</td>
          <td>'.$row['turn'].'</td>
          <td>'.$row['code'].'</td>
          <td><img width="40px" src="'.utf8_encode($row['picture']).'"></td>
          <td><a class="btn orange darken-3" onclick="edit('.$row['code'].');"><i class="material-icons">create</i></a></td>
        </tr>';
  //  }
  }

}
echo $salida;