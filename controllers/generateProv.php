<?php
$_REQUEST['name'];

include '../adapter/connection.php';
require_once('./tcpdf/examples/tcpdf_include.php');



$bars = 'SELECT * FROM users WHERE  code = 100000091 OR code = 100000095 OR code = 100000096'; // consulta de barras solo borrar where para saber que impirmir  no dejar nada para imprimir todo
if(!$bars){
    echo 'Fallo consulta bars por: ' . mysql_error();
    exit;
}
$result = $connection->query($bars);
if(!$result){
    echo 'Fallo consulta result por: ' . mysql_error();
    exit;
}
// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
// set font
$pdf->SetFont('helvetica', '', 10);

// add a page
$pdf->AddPage();
$html = '';
$i = 10;
while ($row = $result->fetch_array()) {

	$params = $pdf->serializeTCPDFtagParameters(array($row['code'], 'C39', $i, '', 80, 35, 0.6, array('position'=>'', 'border'=>true, 'padding'=>4, 'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>true, 'font'=>'helvetica', 'fontsize'=>8, 'stretchtext'=>4), 'N'));
	if ($i == 100) {
		
	$html .= '<tcpdf method="write1DBarcode" params="'.$params.'"/>
				<p style="font-size: large; text-align:right;">'.utf8_encode($row['name']).'</p>';
	}elseif ($i == 10) {
			$html .= '<tcpdf method="write1DBarcode" params="'.$params.'"/>
				<p style="font-size: large;">'.utf8_encode($row['name']).'</p>';
	}
	$i = $i+ 90;
	if ($i == 190) {
		$i = 10;
	}
}
	$pdf->writeHTML($html, true, 0, true, 0);
	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// reset pointer to the last page
$pdf->lastPage();
// ---------------------------------------------------------
//Close and output PDF document
$pdf->Output('codes.pdf', 'I');
//============================================================+
// END OF FILE
//============================================================+