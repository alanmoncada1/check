<?php
include '../model/select.php';


echo '
<table class="responsive-table striped">
    <thead>
      <tr>
          <th>Nombre</th>
          <th>Turno</th>
          <th>Entrada</th>
          <th>Salida</th>
      </tr>
    </thead>

    <tbody>';
    
while ($row = $resultTable->fetch_array()) {
 

     echo ' <tr>
        <td>'.utf8_encode($row['name']).'</td>
        <td>'.$row['turn'].'</td>
        <td>'.$row['login'].'</td>
        <td>'.$row['logout'].'</td>
      </tr>';

}
 echo '
    </tbody>
  </table>';
