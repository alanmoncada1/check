<?php
include '../../adapter/connection.php';
include '../../model/select.php';

header('Content-type: application/vnd.ms-excel');
header("Content-Disposition: attachment; filename=Listado_Quincenal_$fecha.xls");
header("Pragma: no-cache");
header("Expires: 0");

echo '
<table border=0>
    <thead>
      <tr>
          <th>Nombre</th>
          <th>Turno</th>
          <th>Entrada</th>
          <th>Salida</th>
      </tr>
    ';

while ($row = $resultbiweekly->fetch_array()){

     echo '<tr>
        <td>'.$row['name'].'</td>
        <td>'.$row['turn'].'</td>
        <td>'.$row['login'].'</td>
        <td>'.$row['logout'].'</td>
      </tr>';
  
}
 echo '
    </thead>
  </table>';