<?php
session_start();

if(!isset($_SESSION['user'])) {  
  session_destroy();
  echo "<meta content='0;URL=http://callc.podzone.net/check/' http-equiv='REFRESH'> </meta>";
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Reportes</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="../public/icons/policeman.png" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<script type="text/javascript" src="../scripts/table.js"></script>
  <script type="text/javascript" src="../scripts/formUpload.js"></script>
  
<!-- Compiled and minified JavaScript -->
<style type="text/css">
  .down{
    margin-top: 1%;
  }
</style>          
</head>
<body>
  <nav class="orange darken-3">
    <div class="nav-wrapper">
    <a href="#" data-target="slide-out" class="sidenav-trigger"><i class="material-icons">menu</i></a>
  <div class="row">
      <a href="" class="brand-logo center">Dashboard</a>
      <ul id="nav-mobile" class="left hide-on-med-and-down">
        <li><a href="dashboard.php"><i class="material-icons">home</i></a></li>
        <li><a href="#" onclick="incializar()"><i class="material-icons left">backup</i>Ingresar Usuario</a></li>
        <li><a href="reports.php"><i class="material-icons left">assignment</i>Reporte</a></li>
        <li><a href="print.php"><i class="material-icons left">folder_shared</i>Impresion</a></li>
      </ul>

      <ul id="nav-mobile" class="right hide-on-med-and-down">
    <a class="dropdown-trigger btn " data-target='dropdown2'><i class="material-icons left">arrow_drop_down_circle</i>usuario</a>
        
      </ul>
    </div>
    </div>
  </nav>

 <!-- Dropdown Trigger -->

  <!-- Menu desplegable en barra de navegación -->
  <ul id='dropdown1' class='dropdown-content'>
    <li><a href="#!"><i class="material-icons">perm_identity</i>Perfil</a></li>
    <li class="divider" tabindex="-1"></li>
    <li><a href="../controllers/login/logout.php"><i class="material-icons">input</i>Salir</a></li>
  </ul>

 <!-- Menu desplegable en barra de navegación -->
  <ul id='dropdown2' class='dropdown-content'>
    <li><a href="#!"><i class="material-icons">perm_identity</i>Perfil</a></li>
    <li class="divider" tabindex="-1"></li>
    <li><a href="../controllers/login/logout.php"><i class="material-icons">input</i>Salir</a></li>
  </ul>

  <!--Menu de dispositivos moviles-->
  <ul id="slide-out" class="sidenav">
    <li><div class="user-view">
      <div class="background">
        <img src="../public/img/admin.jpg">
      </div>
      <a href="#name"><span class="white-text name"><?php echo $_SESSION['name'];?></span></a>
      <a href="#email"><span class="white-text email"><?php echo $_SESSION['name'];?></span></a>
    </div></li>
    <li><a class="dropdown-trigger" data-target='dropdown1'><i class="material-icons left">arrow_drop_down</i>usuario</a></li>
    <li><a href="#!" onclick="incializar()"><i class="material-icons left">backup</i>Ingresar Usuario</a></li>
    <li><div class="divider"></div></li>
    <li><a class="subheader">Reportería</a></li>
        <li><a href="reports.php"><i class="material-icons left">assignment</i>Reporte</a></li>
        <li><a href="print.php"><i class="material-icons left">folder_shared</i>Impresion</a></li>
  </ul>
<!--
<div class="row">
<div class="input-field"> 
<i class="material-icons prefix">today</i>  
<input type="text" id="icon_prefix" class="datepicker col s2">
</div>

<div class="input-field"> 
<i class="material-icons prefix">today</i>
<input type="text" id="icon_prefix2" class="datepicker col s2">
</div>
<a class="waves-effect waves-light btn"><i class="material-icons left">send</i>Generar</a>
</div>
-->
<div id="table"></div>

<div class="fixed-action-btn">
  <a class="btn-floating btn-large orange darken-4">
    <i class="large material-icons">assignment</i>
  </a>
  <ul>
    <li><a href="../controllers/reports/month.php" class="btn-floating red tooltipped" data-position="left" data-tooltip="Mensual">M</a></li>
    <li><a href="../controllers/reports/biweekly.php" class="btn-floating green tooltipped" data-position="left" data-tooltip="Quincenal">Q</a></li>
    <li><a href="../controllers/reports/week.php" class="btn-floating yellow darken-4 tooltipped" data-position="left" data-tooltip="Semanal">S</a></li>
    <li><a href="../controllers/reports/today.php" class="btn-floating blue tooltipped" data-position="left" data-tooltip="Hoy">H</a></li>
  </ul>
</div>

        



<!-- Modal Structure -->
  <div id="modal1" class="modal">
    <form class="col s12" id="form" enctype="multipart/form-data" method="POST">
    <div class="modal-content">
      <h4>Ingresar nuevo agente</h4>
      <p> <div class="row">
      <div class="row">
        <div class="input-field col s12">
          <input id="name" name="name" type="text" class="validate" required>
          <label for="name">Nombre completo</label>
        </div>
          <div class="input-field col s12">
            <select id="turn" name="turn">
              <option value="" disabled selected>Turno</option>
              <option value="Matutino">Matutino</option>
              <option value="Vespertino">Vespertino</option>
            </select>
            <label>Selecciona un Turno</label>
          </div>

      </div>
      <div class="file-field input-field">
      <div class="btn">
        <span>File</span>
        <input type="file" name="img" class="tooltipped" id="img" data-position="top" data-tooltip="Inserta la imagen de perfil del agente" type="file">
      </div>
      <div class="file-path-wrapper">
        <input class="file-path validate" type="text">
      </div>
    </div>
      </p>
    </div>
    <div class="modal-footer">
      <input type="submit" class="modal-close waves-effect waves-green btn-flat" value="Guardar">
    </div>
      </form>
  </div>



  

<script type="text/javascript">
      function incializar(){
        var elem = document.querySelector('.modal');
        var options = {endingTop:'10%', dismissible: true, outDuration:100, onCloseEnd: function () {
        
        }}
        
        var elemSelect = document.querySelectorAll('select');
        var instances = M.FormSelect.init(elemSelect);
        var instance = M.Modal.init(elem, options);
        var elems = document.querySelectorAll('.tooltipped');
        var instancesTool = M.Tooltip.init(elems);

        instance.open();
    }
</script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js"></script>
  <script type="text/javascript" src="../scripts/materialize.js"></script>

    

</body>
</html>