 $.fn.delayPasteKeyUp = function(fn, ms)
 {
	 var timer = 0;
	 $(this).on("propertychange input", function()
	 {
		 clearTimeout(timer);
		 timer = setTimeout(fn, ms);
	 });
 };
 
 //la utilizamos
 $(document).ready(function()
 {
 	$("#ingreso").delayPasteKeyUp(function(){
	modal($("#ingreso").val());
 }, 200);
 });