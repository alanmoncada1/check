$(document).ready(function(){
	$('.sidenav').sidenav();   
    $('.fixed-action-btn').floatingActionButton();
    $('.dropdown-trigger').dropdown({ coverTrigger: false });
	$('.fixed-action-btn').floatingActionButton();
	$('.tooltipped').tooltip();

});

document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.chips');
    var instances = M.Chips.init(elems);
  });


document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.datepicker');
    var options = {
    	format: 'dd mmm yyyy',
    	 i18n: {
                months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Setiembre", "Octubre", "Noviembre", "Diciembre"],
                monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Set", "Oct", "Nov", "Dic"],
                weekdays: ["Domingo","Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                weekdaysShort: ["Dom","Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
                weekdaysAbbrev: ["D","L", "M", "M", "J", "V", "S"],
                cancel:	'Cancelar',
				clear:	'Limpiar',
				done:	'Ok'
            }
    };
    var instances = M.Datepicker.init(elems, options);
});

