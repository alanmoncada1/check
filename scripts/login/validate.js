function submit(){
	if (form1.user.value == '' || form1.pass.value == ''){
		alert('Tienes que introducir tu nombre y el mensaje');
		return;
	}

	var user = form1.user.value;
	var pass = form1.pass.value;
	var xmlhttp = new XMLHttpRequest();

	xmlhttp.onreadystatechange = function(){
		if (this.readyState==4 && this.status == 200){
			process(this.responseText);
			document.getElementById('response').innerHTML = this.responseText;

			
		}
	}

	xmlhttp.open('GET', '../controllers/login/login.php?user='+user+'&pass='+pass, true);
	xmlhttp.send();
}

var admin = '<div id="admin" class="preloader-wrapper big active"><div class="spinner-layer spinner-blue-only"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>'; 
var suprv = '<div id="super" class="preloader-wrapper big active"><div class="spinner-layer spinner-blue-only"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>';
function process(xml){ 
	if (xml === admin) {
		setTimeout("location.href='dashboard.php'", 3000);
	}else if(xml === suprv){
		setTimeout("location.href='control.php'", 3000);
	}
}